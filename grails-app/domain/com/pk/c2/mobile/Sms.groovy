package com.pk.c2.mobile

class Sms {

    String sender
    String message
    Date messageReceived

    static belongsTo = [device: Device]

    static constraints = {
    }
}
