package com.pk.c2.mobile

class Device {

    def grailsApplication

    String imei
    String imsi
    String phoneNumber
    String voiceMailNumber
    String simSerial
    String networkOperator
    Date lastHeartbeat = new Date()
    List pendingCommands

    static hasMany = [messages: Sms, pendingCommands: String]

    //Transmitting payload: {"number":"15555215554","imsi":"310260000000000","voiceMailNumber":"+15552175049",
    // "simSerial":"89014103211118510720","networkOperator":"310260","imei":"000000000000000"}

    static constraints = {
        imei(blank: false, unique: true, size: 10..30, matches: "[0-9-]+")
    }

    public boolean commandPending(){
        if(this.pendingCommands.size() != 0){
            return true
        }
        else{
            return false
        }
    }

    public String getCommand(){
        def command = this.pendingCommands[0]
        log.debug("Retrieved pending command ${command}")
        //this.pendingCommands.remove(0)
        this.removeFromPendingCommands(command)
        this.save(flush: true)
        log.debug("Remaining pending commands: ${pendingCommands}")
        return command
    }

    public void queueCommand(String command){
        switch (command){
            case 'DUMP_SMS_INBOX':
                queueDumpSmsInbox()
                break
            default:
                // do nothing
                break
        }
    }

    private void queueDumpSmsInbox(){
        log.info("Queueing command: DUMP_SMS_INBOX")
        this.addToPendingCommands("DUMP_SMS_INBOX")
        //this.pendingCommands.add("DUMP_SMS_INBOX")
        log.debug("Pending commands: ${this.pendingCommands}")
        this.save(flush: true)
    }
}
