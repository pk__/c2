package com.pk.c2.mobile

import org.springframework.dao.DataIntegrityViolationException

class SmsController {

    //static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    /**
     * @resource /device/$imei/sms
     * @method GET
     * @param max
     * @return
     */
    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def deviceInstance = Device.findByImei(params.imei)
        def smsInstanceList = deviceInstance.messages
        //[smsInstanceList: Sms.list(params), smsInstanceTotal: Sms.count()]
        // TODO: Order by date
        [smsInstanceList: smsInstanceList, smsInstanceTotal: smsInstanceList.size(), deviceInstance: deviceInstance]
    }

    def create() {
        [smsInstance: new Sms(params)]
    }

    /**
     * @resource /device/$imei/sms
     * @method POST
     * @params
     *  {"messages":[
     *      {"message":"text message yay!","sender":"5551234","messageReceived":"1402614786470"},
     *      {"message":"Test SMS","sender":"123456","messageReceived":"1402535664913"}],
     *   "imei":"000000000000000"}
     * @return
     */
    def save() {
        def deviceInstance = Device.findByImei(params?.imei)
        if(deviceInstance == null){
            log.error("Failed to find device with imei " + params.imei)
            render(status: 404, text: 'Failed to find device with ID ${imei}')
            return
        }

        params.messages.each{
            Date messageReceived = new Date(Long.valueOf(it.messageReceived).longValue())
            if(Sms.findWhere(device: deviceInstance, messageReceived: messageReceived) == null){
                def smsInstance = new Sms(sender: it.sender, message: it.message, messageReceived: messageReceived)
                deviceInstance.addToMessages(smsInstance)
            }
        }

        if (deviceInstance.save(flush: true)) {
            log.info("Sms successfully added to IMEI ${params.imei}")
            render(status: 201, text: 'Sms added successfully')
        }
        else{
            deviceInstance.errors.allErrors.each {
                log.debug(it)
            }
            log.error("Failed to add SMS to device ${params.imei}")
            render(status: 400)
        }
    }

    def show(Long id) {
        def smsInstance = Sms.get(id)
        if (!smsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sms.label', default: 'Sms'), id])
            redirect(action: "list")
            return
        }

        [smsInstance: smsInstance]
    }

    def edit(Long id) {
        def smsInstance = Sms.get(id)
        if (!smsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sms.label', default: 'Sms'), id])
            redirect(action: "list")
            return
        }

        [smsInstance: smsInstance]
    }

    def update(Long id, Long version) {
        def smsInstance = Sms.get(id)
        if (!smsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sms.label', default: 'Sms'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (smsInstance.version > version) {
                smsInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'sms.label', default: 'Sms')] as Object[],
                          "Another user has updated this Sms while you were editing")
                render(view: "edit", model: [smsInstance: smsInstance])
                return
            }
        }

        smsInstance.properties = params

        if (!smsInstance.save(flush: true)) {
            render(view: "edit", model: [smsInstance: smsInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'sms.label', default: 'Sms'), smsInstance.id])
        redirect(action: "show", id: smsInstance.id)
    }

    def delete(Long id) {
        def smsInstance = Sms.get(id)
        if (!smsInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'sms.label', default: 'Sms'), id])
            redirect(action: "list")
            return
        }

        try {
            smsInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'sms.label', default: 'Sms'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'sms.label', default: 'Sms'), id])
            redirect(action: "show", id: id)
        }
    }
}
