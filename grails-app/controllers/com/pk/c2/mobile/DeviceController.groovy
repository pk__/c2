package com.pk.c2.mobile

import org.springframework.dao.DataIntegrityViolationException

class DeviceController {

    //static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [deviceInstanceList: Device.list(params), deviceInstanceTotal: Device.count()]
    }

    def create() {
        [deviceInstance: new Device(params)]
    }

    /**
     * @resource /device/$imei/
     * @method POST
     * @return
     *      201 - Success
     *      400 - Failed
     */
    def save() {
        if(Device.findByImei(params?.imei) != null){
            heartbeat(params)
            return
        }
        def deviceInstance = new Device(params)
        if (!deviceInstance.save(flush: true)) {
            render(status: 400, text: 'Failed to create device with IMEI ${params.imei}')
            return
        }
        else{
            log.info("Successfully created device with IMEI ${params.imei}")
            render(status: 201)
        }
    }

    def show(String imei) {
        def deviceInstance = Device.findByImei(imei)
        if (!deviceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'device.label', default: 'Device'), imei])
            redirect(action: "list")
            return
        }
        [deviceInstance: deviceInstance]
    }

    def edit(Long id) {
        def deviceInstance = Device.get(id)
        if (!deviceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'device.label', default: 'Device'), id])
            redirect(action: "list")
            return
        }

        [deviceInstance: deviceInstance]
    }

    def update(Long id, Long version) {
        def deviceInstance = Device.get(id)
        if (!deviceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'device.label', default: 'Device'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (deviceInstance.version > version) {
                deviceInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'device.label', default: 'Device')] as Object[],
                          "Another user has updated this Device while you were editing")
                render(view: "edit", model: [deviceInstance: deviceInstance])
                return
            }
        }

        deviceInstance.properties = params

        if (!deviceInstance.save(flush: true)) {
            render(view: "edit", model: [deviceInstance: deviceInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'device.label', default: 'Device'), deviceInstance.id])
        redirect(action: "show", id: deviceInstance.id)
    }

    def delete(Long id) {
        def deviceInstance = Device.get(id)
        if (!deviceInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'device.label', default: 'Device'), id])
            redirect(action: "list")
            return
        }

        try {
            deviceInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'device.label', default: 'Device'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'device.label', default: 'Device'), id])
            redirect(action: "show", id: id)
        }
    }

    /**
     *
     * @param params
     * @return Uses the following status codes:
     *      200: Command available
     *      204: Heartbeat accepted, no commands available
     *      404: Device not found
     */
    def heartbeat(params){
        def deviceInstance = Device.findByImei(params?.imei)
        if(deviceInstance != null){
            deviceInstance.lastHeartbeat = new Date()
            deviceInstance.save(flush: true)
            //  TODO: Present any commands that need to be sent to the device
            if(deviceInstance.commandPending()){
                def command = deviceInstance.getCommand()
                render(status: 200, text: command)
            }
            else{
                render(status: 204)
            }
        }
        else{
            log.error("Failed to find device with imei " + params.imei)
            render(status: 404, text: 'Failed to find device with ID ${imei}')
        }
    }

    /**
     * @method POST
     * @resource /device/$imei/command
     * @params command
     */
    def queueCommand(){
        def deviceInstance = Device.findByImei(params?.imei)
        if(deviceInstance != null && params?.command != null){
            deviceInstance.queueCommand(params?.command)
            //flash.message = message(code: 'default.updated.message', args: [message(code: 'device.label', default: 'Device'), deviceInstance.id])
            flash.message = "Command ${params.command} queued successfully"
        }
        else{
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'device.label', default: 'Device'), id])
        }
        redirect(uri: "/device/${deviceInstance.imei}")
    }
}
