
<%@ page import="com.pk.c2.mobile.Device" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'device.label', default: 'Device')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-device" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-device" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list device">
			
				<g:if test="${deviceInstance?.imei}">
				<li class="fieldcontain">
					<span id="imei-label" class="property-label"><g:message code="device.imei.label" default="Imei" /></span>
					
						<span class="property-value" aria-labelledby="imei-label"><g:fieldValue bean="${deviceInstance}" field="imei"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deviceInstance?.imsi}">
				<li class="fieldcontain">
					<span id="imsi-label" class="property-label"><g:message code="device.imsi.label" default="Imsi" /></span>
					
						<span class="property-value" aria-labelledby="imsi-label"><g:fieldValue bean="${deviceInstance}" field="imsi"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deviceInstance?.lastHeartbeat}">
				<li class="fieldcontain">
					<span id="lastHeartbeat-label" class="property-label"><g:message code="device.lastHeartbeat.label" default="Last Heartbeat" /></span>
					
						<span class="property-value" aria-labelledby="lastHeartbeat-label"><g:formatDate date="${deviceInstance?.lastHeartbeat}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${deviceInstance?.messages}">
				<li class="fieldcontain">
					<span id="messages-label" class="property-label"><g:message code="device.messages.label" default="Messages" /></span>
                        <g:if test="${deviceInstance.messages.size() != 0}">
                            <span class="property-value" aria-labelledby="messages-label"><a href="/C2/device/${deviceInstance?.imei}/sms">SMS Messages</a></span>
                        </g:if>
				</li>
				</g:if>
			
				<g:if test="${deviceInstance?.networkOperator}">
				<li class="fieldcontain">
					<span id="networkOperator-label" class="property-label"><g:message code="device.networkOperator.label" default="Network Operator" /></span>
					
						<span class="property-value" aria-labelledby="networkOperator-label"><g:fieldValue bean="${deviceInstance}" field="networkOperator"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deviceInstance?.phoneNumber}">
				<li class="fieldcontain">
					<span id="phoneNumber-label" class="property-label"><g:message code="device.phoneNumber.label" default="Phone Number" /></span>
					
						<span class="property-value" aria-labelledby="phoneNumber-label"><g:fieldValue bean="${deviceInstance}" field="phoneNumber"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deviceInstance?.simSerial}">
				<li class="fieldcontain">
					<span id="simSerial-label" class="property-label"><g:message code="device.simSerial.label" default="Sim Serial" /></span>
					
						<span class="property-value" aria-labelledby="simSerial-label"><g:fieldValue bean="${deviceInstance}" field="simSerial"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${deviceInstance?.voiceMailNumber}">
				<li class="fieldcontain">
					<span id="voiceMailNumber-label" class="property-label"><g:message code="device.voiceMailNumber.label" default="Voice Mail Number" /></span>
					
						<span class="property-value" aria-labelledby="voiceMailNumber-label"><g:fieldValue bean="${deviceInstance}" field="voiceMailNumber"/></span>
					
				</li>
				</g:if>
			</ol>

            <h1>Device Commands</h1>
            <ol class="property-list device">
                <li class="fieldcontain">
                    <span id="commandPending-label" class="property-label">Commands Pending</span>
                    <span class="property-value" aria-labelledby="commandPending-label">${deviceInstance.pendingCommands.size()}</span>
                </li>

                <li class="fieldcontain">
                    <span id="commands-label" class="property-label">Commands</span>
                    <span class="property-value" aria-labelledby="commands-label">
                        <form id="commandForm" action="/C2/device/${deviceInstance.imei}/command" method="post">
                            <button type="submit" name="command" value="DUMP_SMS_INBOX" form="commandForm">Dump SMS Inbox</button>
                        </form>
                    </span>

                </li>
            </ol>

            <div>

            </div>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${deviceInstance?.id}" />
					<g:link class="edit" action="edit" id="${deviceInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
