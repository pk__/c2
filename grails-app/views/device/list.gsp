
<%@ page import="com.pk.c2.mobile.Device" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'device.label', default: 'Device')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-device" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
            </ul>
		</div>
		<div id="list-device" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="imei" title="${message(code: 'device.imei.label', default: 'Imei')}" />
					
						<g:sortableColumn property="imsi" title="${message(code: 'device.imsi.label', default: 'Imsi')}" />
					
						<g:sortableColumn property="lastHeartbeat" title="${message(code: 'device.lastHeartbeat.label', default: 'Last Heartbeat')}" />
					
						<g:sortableColumn property="networkOperator" title="${message(code: 'device.networkOperator.label', default: 'Network Operator')}" />
					
						<g:sortableColumn property="phoneNumber" title="${message(code: 'device.phoneNumber.label', default: 'Phone Number')}" />

					</tr>
				</thead>
				<tbody>
				<g:each in="${deviceInstanceList}" status="i" var="deviceInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
					    <td><a href="/C2/device/${deviceInstance.imei}">${fieldValue(bean: deviceInstance, field: "imei")}</a></td>
						<td>${fieldValue(bean: deviceInstance, field: "imsi")}</td>
					
						<td><g:formatDate date="${deviceInstance.lastHeartbeat}" /></td>
					
						<td>${fieldValue(bean: deviceInstance, field: "networkOperator")}</td>
					
						<td>${fieldValue(bean: deviceInstance, field: "phoneNumber")}</td>

					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${deviceInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
