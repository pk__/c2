<%@ page import="com.pk.c2.mobile.Device" %>



<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'imei', 'error')} required">
	<label for="imei">
		<g:message code="device.imei.label" default="Imei" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="imei" maxlength="30" pattern="${deviceInstance.constraints.imei.matches}" required="" value="${deviceInstance?.imei}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'imsi', 'error')} ">
	<label for="imsi">
		<g:message code="device.imsi.label" default="Imsi" />
		
	</label>
	<g:textField name="imsi" value="${deviceInstance?.imsi}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'lastHeartbeat', 'error')} required">
	<label for="lastHeartbeat">
		<g:message code="device.lastHeartbeat.label" default="Last Heartbeat" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="lastHeartbeat" precision="day"  value="${deviceInstance?.lastHeartbeat}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'messages', 'error')} ">
	<label for="messages">
		<g:message code="device.messages.label" default="Messages" />
		
	</label>
	
<ul class="one-to-many">
<g:each in="${deviceInstance?.messages?}" var="m">
    <li><g:link controller="sms" action="show" id="${m.id}">${m?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="sms" action="create" params="['device.id': deviceInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'sms.label', default: 'Sms')])}</g:link>
</li>
</ul>

</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'networkOperator', 'error')} ">
	<label for="networkOperator">
		<g:message code="device.networkOperator.label" default="Network Operator" />
		
	</label>
	<g:textField name="networkOperator" value="${deviceInstance?.networkOperator}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'phoneNumber', 'error')} ">
	<label for="phoneNumber">
		<g:message code="device.phoneNumber.label" default="Phone Number" />
		
	</label>
	<g:textField name="phoneNumber" value="${deviceInstance?.phoneNumber}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'simSerial', 'error')} ">
	<label for="simSerial">
		<g:message code="device.simSerial.label" default="Sim Serial" />
		
	</label>
	<g:textField name="simSerial" value="${deviceInstance?.simSerial}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: deviceInstance, field: 'voiceMailNumber', 'error')} ">
	<label for="voiceMailNumber">
		<g:message code="device.voiceMailNumber.label" default="Voice Mail Number" />
		
	</label>
	<g:textField name="voiceMailNumber" value="${deviceInstance?.voiceMailNumber}"/>
</div>

