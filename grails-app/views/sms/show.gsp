
<%@ page import="com.pk.c2.mobile.Sms" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'sms.label', default: 'Sms')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-sms" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><a class="list" href="/C2/device">Device List</a></li>
                <li><a class="device" href="/C2/device/${smsInstance?.device?.imei}">Show Device</a></li>
                <li><a class="sms" href="/C2/device/${smsInstance?.device?.imei}/sms"><g:message code="default.list.label" args="[entityName]" /></a></li>
			</ul>
		</div>
		<div id="show-sms" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list sms">
			
				<g:if test="${smsInstance?.device}">
				<li class="fieldcontain">
					<span id="device-label" class="property-label"><g:message code="sms.device.label" default="Device" /></span>
					
                    <span class="property-value" aria-labelledby="device-label"><a href="/C2/device/${smsInstance?.device?.imei}">${smsInstance?.device?.imei.encodeAsHTML()}</a></span>


                </li>
				</g:if>
			
				<g:if test="${smsInstance?.message}">
				<li class="fieldcontain">
					<span id="message-label" class="property-label"><g:message code="sms.message.label" default="Message" /></span>
					
						<span class="property-value" aria-labelledby="message-label"><g:fieldValue bean="${smsInstance}" field="message"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${smsInstance?.messageReceived}">
				<li class="fieldcontain">
					<span id="messageReceived-label" class="property-label"><g:message code="sms.messageReceived.label" default="Message Received" /></span>
					
						<span class="property-value" aria-labelledby="messageReceived-label"><g:formatDate date="${smsInstance?.messageReceived}" /></span>
					
				</li>
				</g:if>
			
				<g:if test="${smsInstance?.sender}">
				<li class="fieldcontain">
					<span id="sender-label" class="property-label"><g:message code="sms.sender.label" default="Sender" /></span>
					
						<span class="property-value" aria-labelledby="sender-label"><g:fieldValue bean="${smsInstance}" field="sender"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${smsInstance?.id}" />
					<g:link class="edit" action="edit" id="${smsInstance?.id}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
