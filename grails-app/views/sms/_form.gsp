<%@ page import="com.pk.c2.mobile.Sms" %>



<div class="fieldcontain ${hasErrors(bean: smsInstance, field: 'device', 'error')} required">
	<label for="device">
		<g:message code="sms.device.label" default="Device" />
		<span class="required-indicator">*</span>
	</label>
	<g:select id="device" name="device.id" from="${com.pk.c2.mobile.Device.list()}" optionKey="id" required="" value="${smsInstance?.device?.id}" class="many-to-one"/>
</div>

<div class="fieldcontain ${hasErrors(bean: smsInstance, field: 'message', 'error')} ">
	<label for="message">
		<g:message code="sms.message.label" default="Message" />
		
	</label>
	<g:textField name="message" value="${smsInstance?.message}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: smsInstance, field: 'messageReceived', 'error')} required">
	<label for="messageReceived">
		<g:message code="sms.messageReceived.label" default="Message Received" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="messageReceived" precision="day"  value="${smsInstance?.messageReceived}"  />
</div>

<div class="fieldcontain ${hasErrors(bean: smsInstance, field: 'sender', 'error')} ">
	<label for="sender">
		<g:message code="sms.sender.label" default="Sender" />
		
	</label>
	<g:textField name="sender" value="${smsInstance?.sender}"/>
</div>

