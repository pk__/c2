
<%@ page import="com.pk.c2.mobile.Sms" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'sms.label', default: 'Sms')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-sms" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><a class="list" href="/C2/device">Device List</a></li>
                <li><a class="device" href="/C2/device/${deviceInstance?.imei}">Show Device</a></li>
            </ul>
		</div>
		<div id="list-sms" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
                        <g:sortableColumn property="messageReceived" title="${message(code: 'sms.messageReceived.label', default: 'Message Received')}" />
                        <g:sortableColumn property="sender" title="${message(code: 'sms.sender.label', default: 'Sender')}" />
                        <g:sortableColumn property="message" title="${message(code: 'sms.message.label', default: 'Message')}" />
					</tr>
				</thead>
				<tbody>
				<g:each in="${smsInstanceList}" status="i" var="smsInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">

                        <td><a href="/C2/device/${deviceInstance?.imei}/sms/${smsInstance.id}"><g:formatDate date="${smsInstance.messageReceived}" /></a></td>

                        <td>${fieldValue(bean: smsInstance, field: "sender")}</td>

						<td>${fieldValue(bean: smsInstance, field: "message")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${smsInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
